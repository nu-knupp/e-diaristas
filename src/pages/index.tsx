import type { NextPage } from 'next'
import SafeEnviroment from 'ui/components/feedback/SafeEnviroment/SafeEnviroment'
import PageTitle from 'ui/components/data-display/PageTitle/PageTitle'
import UserInformation from 'ui/components/data-display/UserInformation/UserInformation'

const Home: NextPage = () => {
  return (
    <div>
      <SafeEnviroment />
      <PageTitle 
        title={'Conheça os profissionais'}
        subtitle={'Preencha seu endereço e veja todos os profissionais da sua localidade'}
      />
      <UserInformation 
        name={'Nubia'}
        picture={''}
        rating={3}
        description={'São joão'}
      />
    </div>
  )
}

export default Home
